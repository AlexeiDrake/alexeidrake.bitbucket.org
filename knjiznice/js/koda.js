
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var users = [
	{
		name: "Mark Bizilj",
		ehrid: "4f922c7a-f92e-4a60-acb1-c16f6d04e943"
	},
	{
		name: "Uroš Breskvar",
		ehrid: "c395effd-e54c-4df9-b0aa-df05b662c01f"
	},
	{
		name: "Marija Terezija",
		ehrid: "3f790fdb-38f7-4ac4-85ad-038c09b0bb0b"
	},
	{
		name: "Živa Groza",
		ehrid: "59639a6c-94fb-4f72-869d-d797c03ecea2"
	}
];

function posodobiUporabnike() {
	$('select').html('<option></option>');
	for (var i in users) {
		var user = users[i];
		$('select').append('<option value="' + user.ehrid + '">' + user.name + '</option>');
	}
}

$(document).ready(function() {
	posodobiUporabnike();
});


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
	var response = $.ajax({
		type: "POST",
		url: baseUrl + "/session?username=" + encodeURIComponent(username) +
				"&password=" + encodeURIComponent(password),
		async: false
	});
	return response.responseJSON.sessionId;
}

function ustvariEHR(ime, priimek, datumRojstva) {
	var result = { message: undefined, ehrid: undefined };
	sessionId = getSessionId();
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
		url: baseUrl + "/ehr",
		type: 'POST',
		async: false,
		success: function (data) {
			var ehrId = data.ehrId;
			var partyData = {
				firstNames: ime,
				lastNames: priimek,
				dateOfBirth: new Date(datumRojstva).toISOString(),
				partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
			};
			$.ajax({
				url: baseUrl + "/demographics/party",
				type: 'POST',
				async: false,
				contentType: 'application/json',
				data: JSON.stringify(partyData),
				success: function (party) {
					if (party.action == 'CREATE') {
						result.message = "<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" +
							ehrId + "'.</span>";
					}
					result.ehrid = ehrId;
				},
				error: function(err) {
					result.message = "<span class='obvestilo label label-danger fade-in'>Napaka '" +
						JSON.parse(err.responseText).userMessage + "'!";
				}
			});
		}
	});
	return result;
}

/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
		priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
		"label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var res = ustvariEHR(ime, priimek, datumRojstva);
		if (res.message) {
			$("#kreirajSporocilo").html(res.message);
		}
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
		"fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
			success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
			"label-success fade-in'>Bolnik '" + party.firstNames + " " +
			party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
			"'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
					"label-danger fade-in'>Napaka '" +
					JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function posljiUtrip(ehrId, datumInUra, srcniUtrip) {
	sessionId = getSessionId();
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	var podatki = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": datumInUra,
		"vital_signs/pulse/any_event/rate": srcniUtrip,
	};
	var parametriZahteve = {
		ehrId: ehrId,
		templateId: 'Vital Signs',
		format: 'FLAT',
	};
	var error = undefined;
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parametriZahteve),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(podatki),
		success: function (res) {
			error = "<span class='obvestilo label label-success fade-in'>" +
				res.meta.href + ".</span>";
		},
		error: function(err) {
			error = "<span class='obvestilo label label-danger fade-in'>Napaka '" +
				JSON.parse(err.responseText).userMessage + "'!";
		}
	});
	return error;
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var srcniUtrip = $("#dodajVitalnoSrcniUtrip").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
		"label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var error = posljiUtrip(ehrId, datumInUra, srcniUtrip);
		if (error) {
			$("#dodajMeritveVitalnihZnakovSporocilo").html(error);
		}
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
				"label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
			success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
						"podatkov za bolnika <b>'" + party.firstNames +
						" " + party.lastNames + "'</b>.</span><br/><br/>");
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "pulse?limit=100",
					type: 'GET',
					headers: {"Ehr-Session": sessionId},
					success: function (res) {
						if (res.length > 0) {
							var results = "<table class='table table-striped " +
									"table-hover'><tr><th>Datum in ura</th>" +
									"<th class='text-right'>Srčni utrip</th></tr>";
							for (var i in res) {
								results += "<tr><td>" + res[i].time +
										"</td><td class='text-right'>" + res[i].pulse +
										res[i].unit + "</td></tr>";
							}
							results += "</table>";
							$("#rezultatMeritveVitalnihZnakov").append(results);
						} else {
							$("#preberiMeritveVitalnihZnakovSporocilo").html(
									"<span class='obvestilo label label-warning fade-in'>" +
									"Ni podatkov!</span>");
						}
					},
					error: function() {
						$("#preberiMeritveVitalnihZnakovSporocilo").html(
								"<span class='obvestilo label label-danger fade-in'>Napaka '" +
				JSON.parse(err.responseText).userMessage + "'!");
					}
				});
			},
			error: function(err) {
				$("#preberiMeritveVitalnihZnakovSporocilo").html(
						"<span class='obvestilo label label-danger fade-in'>Napaka '" +
						JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function izracunVO2() {
	sessionId = getSessionId();

	var ehrId = $("#izracunVO2EHRid").val();

	$("#izracunVO2Sporocilo").html("");
	if (!ehrId || ehrId.trim().length == 0) {
		$("#izracunVO2Sporocilo").html("<span class='obvestilo " +
				"label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
			success: function (data) {
				var party = data.party;
				$("#rezultatVO2").html("<br/><span>Pridobivanje " +
						"podatkov za bolnika <b>'" + party.firstNames +
						" " + party.lastNames + "'</b>.</span><br/><br/>");
				var HRrest = 0;
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "pulse?limit=100",
					type: 'GET',
					headers: {"Ehr-Session": sessionId},
					success: function (res) {
						if (res.length > 0) {
							var days = {};
							for (var i in res) {
								var date = new Date(res[i].time).toISOString().substring(0, 10);
								if (!days[date]) {
									days[date] = { count: 1, pulse: res[i].pulse };
								} else {
									var oldCount = days[date].count;
									var newCount = oldCount + 1;
									var newPulse = (days[date].pulse * oldCount + res[i].pulse) / newCount;
									days[date] = { count: newCount, pulse: newPulse };
								}
							}
							var orderedKeys = [];
							var orderedVals = [];
							var count = Object.keys(days).length;
							var sliceIndex = count > 5 ? count - 5 : 0;
							Object.keys(days).sort().slice(sliceIndex).forEach(function(day) {
								var age = (new Date(day)).getFullYear() - new Date(party.dateOfBirth).getFullYear();
								var HRmax = 205.8 - 0.685 * age;
								var HRrest = days[day].pulse;
								var VO2max = Math.round(15.3 * HRmax / HRrest);
								orderedKeys.push(day);
								orderedVals.push(VO2max);
							});

							var ctx = $('#VO2graf').get(0).getContext('2d');
							var myChart = new Chart(ctx, {
								type: 'line',
								data: {
									labels: orderedKeys,
									datasets: [{
										label: 'VO2 max',
										data: orderedVals,
										backgroundColor: [
											'rgba(255, 159, 64, 0.2)'
										],
										borderColor: [
											'rgba(255, 159, 64, 1)'
										],
										borderWidth: 1
									}]
								},
								options: {
									scales: {
										yAxes: [{
											ticks: {
												suggestedMin: 30,
												suggestedMax: 60,
												beginAtZero: false
											}
										}]
									}
								}
							});
							
						} else {
							$("#izracunVO2Sporocilo").html(
									"<span class='obvestilo label label-warning fade-in'>" +
									"Ni podatkov!</span>");
						}
					},
					error: function() {
						$("#izracunVO2Sporocilo").html(
								"<span class='obvestilo label label-danger fade-in'>Napaka '" +
				JSON.parse(err.responseText).userMessage + "'!");
					}
				});
			},
			error: function(err) {
				$("#preberiMeritveVitalnihZnakovSporocilo").html(
						"<span class='obvestilo label label-danger fade-in'>Napaka '" +
						JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


$(document).ready(function() {

	/**
	 * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
	 * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
	 * padajočega menuja (npr. Pujsa Pepa).
	 */
	$('#preberiPredlogoBolnika').change(function() {
		$("#kreirajSporocilo").html("");
		var podatki = $(this).val().split(",");
		$("#kreirajIme").val(podatki[0]);
		$("#kreirajPriimek").val(podatki[1]);
		$("#kreirajDatumRojstva").val(podatki[2]);
	});

	/**
	 * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
	 * ko uporabnik izbere vrednost iz padajočega menuja
	 * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
	 */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

	/**
	 * Napolni testne vrednosti (EHR ID, datum in ura, srcni utrip)
	 * pri vnosu meritve vitalnih znakov bolnika, ko uporabnik izbere vrednosti iz
	 * padajočega menuja (npr. Ata Smrk)
	 */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		$("#dodajVitalnoEHR").val($(this).val());
	});

	/**
	 * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
	 * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
	 * (npr. Ata Smrk, Pujsa Pepa)
	 */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
	
	$('#preberiObstojeciEHRVO2').change(function() {
		$("#izracunVO2Sporocilo").html("");
		$("#izracunVO2EHRid").val($(this).val());
	});
});

Date.prototype.addDays = function(days) {
	var dat = new Date(this.valueOf());
	dat.setDate(dat.getDate() + days);
	return dat;
}

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

String.prototype.capitalise = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

function generirajPodatke() {
	var newUsers = [];
	$.ajax({
		url: 'https://randomuser.me/api/?results=3',
		dataType: 'json',
		success: function(data) {
			data.results.forEach(function(datum) {
				var ime = datum.name.first.capitalise();
				var priimek = datum.name.last.capitalise();
				var datumRojstva = datum.dob;
				var ehr = ustvariEHR(ime, priimek, datumRojstva);
				var user = {
					name: ime + ' ' + priimek,
					ehrid: ehr.ehrid
				};
				newUsers.push(user);
			});
			users = newUsers;
			posodobiUporabnike();
			for (var u in users) {
				var user = users[u];
				var day = new Date().addDays(-5);
				for (var iday = 0; iday < 5; ++iday) {
					for (var itime = 0; itime < 1; ++itime) {
						day.setHours(getRandomInt(0, 23), getRandomInt(0, 59), getRandomInt(0, 59));
						var heartRate = 60 + getRandomInt(-5, 30);
						posljiUtrip(user.ehrid, day.toISOString(), heartRate);
					}
					day = day.addDays(1);
				}
			}
		}
	});
}
